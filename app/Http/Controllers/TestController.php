<?php

namespace App\Http\Controllers;

use App\Product;
// use Barryvdh\DomPDF\PDF;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

// use PDF;

class TestController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('frontend.test.index', compact('products'));
    }

    // Generate PDF
    public function createPDF()
    {
        $product = Product::all();

        $pdf = PDF::loadview('product', $product);
        return $pdf->stream();
        return view('frontend.test.pdf');
        // return $pdf->download('laporan-product-pdf');
    }
}
