<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class ProductController extends Controller
{
     public function index()
    {
        $product = Product::all();
        $category = Category::all();
        return view('admin.product.index', compact('product','category'));
    }

        // Memasukkan produk baru
    public function store(Request $request)
    {

$filenameWithExt = $request->gambarproduk->getClientOriginalName ();// Get Filename
$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);// Get just Extension
$extension = $request->gambarproduk->getClientOriginalExtension();// Filename To store
$fileNameToStore = $filename.'.'.$extension;
$request->gambarproduk->move(public_path(''), $fileNameToStore);

        Product::create([
            'name' => $request->namaproduk,
            'category_id' => $request->kategoriproduk,
            'description' => $request->deskripsiproduk,
            'small_description' => $request->deskripsikecilproduk,
            'quantity' => $request->jumlahproduk,
            'price' => $request->hargaproduk,
            'image' => $fileNameToStore,
        ]);
        return redirect('/dataproduct');
    }



        //Update produk yang sudah ada
    public function updateproduk($id,  Request $request)
    {

        
        $produk = Product::find($id);
        $produk->name = $request->namaproduk;
        $produk->category_id = $request->kategoriproduk;
        $produk->description = $request->deskripsiproduk;
        $produk->small_description = $request->deskripsikecilproduk;
        $produk->quantity = $request->jumlahproduk;
        $produk->price = $request->hargaproduk;
        $produk->update();
        return redirect('/dataproduct');
    }

        public function destroy($id)
    {
        $produk = Product::find($id);
        $produk->delete();
        return redirect('/product');
    }


}
