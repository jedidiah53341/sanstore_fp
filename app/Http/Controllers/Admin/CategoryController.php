<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\User;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('admin.category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   // Memasukkan data baru
    public function store(Request $request)
    {

$filenameWithExt = $request->gambarkategori->getClientOriginalName ();// Get Filename
$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);// Get just Extension
$extension = $request->gambarkategori->getClientOriginalExtension();// Filename To store
$fileNameToStore = $filename.'.'.$extension;
$request->gambarkategori->move(public_path('images/category'), $fileNameToStore);

        Category::create([
            'name' => $request->namakategori,
            'description' => $request->deskripsikategori,
            'image' => $fileNameToStore
        ]);
        return redirect('/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Category::find($id);
        $kategori->delete();

        $products = Product::where('category_id',$kategori->id)->get();

        foreach($products as $product) {
            $product->delete();
        }
        return back();
    }

    //Update kategori yang sudah ada
    public function updatekategori($id,  Request $request)
    {

        
        $kategori = Category::find($id);
        $kategori->name = $request->namakategori;
        $kategori->description = $request->deskripsikategori;

        if ($request->gambarkategori) {
        $filenameWithExt = $request->gambarkategori->getClientOriginalName ();// Get Filename
$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);// Get just Extension
$extension = $request->gambarkategori->getClientOriginalExtension();// Filename To store
$fileNameToStore = $filename.'.'.$extension;
$request->gambarkategori->move(public_path('images/category'), $fileNameToStore);

        $kategori->image = $fileNameToStore;
        }

        $kategori->update();
        return redirect('/category');
    }
    
}
